import {KeysModule} from './keys/keys.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(KeysModule, 'keys');
})();
