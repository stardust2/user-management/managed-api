import {Module} from '@nestjs/common';
import {KeysController} from 'src/keys/keys.controller';
import {KeysService} from 'src/keys/keys.service';

@Module({
    controllers: [KeysController],
    providers: [KeysService],
})
export class KeysModule {}
