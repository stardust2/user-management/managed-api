import {Controller, Get, Query, Post, Body, UseGuards} from '@nestjs/common';
import {KeysService} from 'src/keys/keys.service';

import {AuthGuard} from '@nestjs/passport';
import {Guards, User, Pipes, Types, StatusController} from 'src/shared';

@Controller('keys')
export class KeysController extends StatusController {
    constructor(private readonly keysService: KeysService) {
        super(['db']);
    }

    @Get('list')
    async getKeys(@Query('username', Pipes.UserExistsPipe) user: User): Promise<Types.KeysObj> {
        return this.keysService.getKeys(user);
    }

    /**
     * TODO: Check key can be generated first
     */
    @UseGuards(AuthGuard('local'))
    @Post('generate')
    async generateKey(@Body('username', Pipes.UserExistsPipe) user: User, @Body('opts') opts: Types.KeyGenOptions): Promise<Types.Key> {
        return this.keysService.generateAndStoreKey(user, opts);
    }

    @UseGuards(new Guards.InternalCallGuard())
    @Post('generate_internal')
    async generateKeyInternal(@Body('username', Pipes.UserExistsPipe) user: User, @Body('opts') opts: Types.KeyGenOptions): Promise<{ key: Types.Key }> {
        const key = await this.keysService.generateAndStoreKey(user, opts);
        return {key};
    }

    @UseGuards(AuthGuard('local'))
    @Post('remove')
    async removeKey(@Body('username', Pipes.UserExistsPipe) user: User, @Body('key', Pipes.KeyExistsPipe) key: Types.Key): Promise<boolean> {
        return this.keysService.removeKey(user, key);
    }
}
