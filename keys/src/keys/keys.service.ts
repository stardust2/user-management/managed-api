import {Injectable} from '@nestjs/common';

import {Keys, User, Utils, Types} from 'src/shared';

@Injectable()
export class KeysService {
    async getKeys(user: User): Promise<Types.KeysObj> {
        return Utils.callInternalGET('db', 'user_getValue', {user, key: 'keys'});
    }

    async generateKey(user: User): Promise<Types.Key> {
        return Keys.generateKeyString(user);
    }

    async generateAndStoreKey(
        user: User,
        opts: Types.KeyGenOptions = {uses: Infinity, ttl: Infinity, scope: Types.Scope.FULL, end: Infinity},
    ): Promise<Types.Key> {
        const newKey = await this.generateKey(user);
        await Utils.callInternalPOST('db', 'key_add', {user, key: newKey, opts});
        return newKey;
    }

    async removeKey(user: User, key: Types.Key): Promise<boolean> {
        return Utils.callInternalPOST('db', 'key_remove', {user, key});
    }
}
