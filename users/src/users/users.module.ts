import {Module} from '@nestjs/common';
import {UsersController} from 'src/users/users.controller';
import {UsersService} from 'src/users/users.service';
import {LocalStrategy} from 'src/shared';

@Module({
    controllers: [UsersController],
    providers: [UsersService, LocalStrategy],
})
export class UsersModule {}
