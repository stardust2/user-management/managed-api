import {Injectable} from '@nestjs/common';

import {Types, User, UUID, Utils} from 'src/shared';

@Injectable()
export class UsersService {
    async initUser(userData: User): Promise<boolean> {
        const {uuid, username} = userData;
        const valid1 = (await Utils.callInternalPOST('db', 'user_update', {
            user: uuid,
            data: {username: username, keys: {}, uuid},
        })) as boolean;
        const valid2 = (await Utils.callInternalPOST('db', 'user_setValue', {user: username, key: 'uuid', data: uuid})) as boolean;
        return valid1 && valid2;
    }

    async signup(userData: User, password: Types.Password): Promise<{ uuid: Types.UUID; key: Types.Key }> {
        const uuid = UUID.generate();
        const user = userData.merge({uuid});
        await this.initUser(user);
        const {username, email} = userData;
        await Utils.callInternalPOST('passwords', 'store', {username, password});
        await Utils.callInternalPOST('emails', 'store', {username, email});
        const {key} = await Utils.callInternalPOST('keys', 'generate_internal', {username});
        return {uuid, key};
    }

    async lookup(user: Types.MaybeUser): Promise<Types.UserData> {
        const data = (await Utils.callInternalGET('db', 'user_getValue', {user, key: '*'})) as Types.UserData;
        return data;
    }

    async storeData<T>(user: Types.MaybeUser, data: T): Promise<boolean> {
        return Utils.callInternalPOST('db', 'user_setValue', {user, key: 'misc', data});
    }
}
