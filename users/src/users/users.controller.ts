import {Controller, Post, Body, Get, Query, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {UsersService} from 'src/users/users.service';

import {User, Types, Pipes, StatusController} from 'src/shared';

@Controller('users')
export class UsersController extends StatusController {
    constructor(protected readonly usersService: UsersService) {
        super(['db', 'passwords', 'emails', 'keys']);
    }

    @Post('signup') async signup(
        @Body('password', Pipes.ParsePasswordPipe) password: Types.Password,
        @Body('username', Pipes.UserNotExistsPipe) user: User,
        @Body('email', Pipes.ParseEmailPipe, Pipes.EmailNotExistsPipe) email: User,
    ): Promise<{ uuid: Types.UUID; key: Types.Key }> {
        return this.usersService.signup(user.merge(email), password);
    }

    @Get('data') async data(@Query('username', Pipes.UserExistsPipe) user: User): Promise<Types.UserData> {
        return this.usersService.lookup(user);
    }

    @Get('data_uuid') async dataUUID(@Query('username') user: User): Promise<Types.UserData> {
        return this.usersService.lookup(user);
    }

    @UseGuards(AuthGuard('local'))
    @Post('store')
    async storeData<T>(@Body('username', Pipes.UserExistsPipe) user: User, @Body('data') data: T): Promise<boolean> {
        return this.usersService.storeData(user, data);
    }
}
