import {UsersModule} from './users/users.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(UsersModule, 'users');
})();
