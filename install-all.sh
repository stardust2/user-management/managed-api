FILES=(*/package.json)

for FILE in ${FILES[*]}
do
  DIR=$(dirname $FILE);
  (cd $DIR && npm i)
done

for FILE in ${FILES[*]/shared\/package.json}
do
  DIR=$(dirname $FILE);
  ln -s ../../shared $DIR/src/shared
done
