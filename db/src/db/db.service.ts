import {Injectable} from '@nestjs/common';
import {RedisService} from 'nestjs-redis';

import {User, Types, config} from 'src/shared';
import * as IORedis from 'ioredis';

const isOk = (x) => x === 'OK';
const allTrue = (x: boolean[]) => x.every((y) => y);

const {UUID: UUID_ROOTKEY, userData: USER_DATA_ROOTKEY, keySet: KEY_SET_ROOTKEY} = config.redis.roots;

@Injectable()
export class DBService {
    protected client: IORedis.Redis;

    constructor(protected readonly redisService: RedisService) {
        this.client = this.redisService.getClient();
    }

    protected async getData<T>(name: string, key: string): Promise<T> {
        const data = await this.client.hmget(name, key).then((x) => x[0] as string);
        return JSON.parse(data || '{}');
    }

    protected async setData<T>(name: string, key: string, data: T): Promise<boolean> {
        return this.client.hmset(name, key, JSON.stringify(data)).then(isOk);
    }

    protected async updateData<T>(name: string, key: string, data: T): Promise<boolean> {
        const oldData = await this.getData<T>(name, key);
        return this.setData(name, key, {...oldData, ...data});
    }

    protected async getUserData(user: Types.MaybeUser): Promise<Types.UserData> {
        return this.getData(USER_DATA_ROOTKEY, await this.getUUID(user));
    }

    protected async getUUID(user: Types.MaybeUser): Promise<Types.UUID> {
        if (User.isUUID(user)) {
            return user;
        }
        if (User.isUsername(user) || User.isEmail(user)) {
            return this.client.hmget(UUID_ROOTKEY, user).then((x) => x[0] as Types.UUID);
        }
        return user.uuid;
    }

    protected async removeUUID(key: Types.Email | Types.Username): Promise<boolean> {
        return this.client.hdel(UUID_ROOTKEY, key).then(Boolean);
    }

    protected async keyExists(key: Types.Key): Promise<boolean> {
        return this.client.sismember(KEY_SET_ROOTKEY, key).then(Boolean);
    }

    protected async uuidExists(uuid: Types.UUID): Promise<boolean> {
        return this.client.sismember(USER_DATA_ROOTKEY, uuid).then(Boolean);
    }

    protected async usernameExists(username: Types.Username): Promise<boolean> {
        return this.client.hexists(UUID_ROOTKEY, username).then(Boolean);
    }

    protected async emailExists(email: Types.Email): Promise<boolean> {
        return this.client.hexists(UUID_ROOTKEY, email).then(Boolean);
    }

    protected async storeUUID(key: Types.Email | Types.Username, uuid: Types.UUID): Promise<boolean> {
        if (uuid === '') {
            return this.removeUUID(key);
        }
        return this.client.hmset(UUID_ROOTKEY, key, uuid).then(isOk);
    }

    async flush(): Promise<boolean> {
        return this.client.flushdb().then(isOk);
    }

    async updateUserData(user: Types.MaybeUser, data: Partial<Types.UserData>): Promise<boolean> {
        return this.updateData(USER_DATA_ROOTKEY, await this.getUUID(user), data);
    }

    async setUserDataValue<T extends Types.UserDataKey>(user: Types.MaybeUser, key: T, data: Types.UserData[T]) {
        if (key === 'uuid') {
            this.storeUUID(user as Types.Email | Types.Username, data);
        }
        return this.updateUserData(user, {[key]: data});
    }

    async getUserDataValue<T extends Types.UserDataKey>(user: Types.MaybeUser, key: T): Promise<Types.UserData[T]> {
        if (key === '*') {
            return this.getUserData(user);
        }
        return this.getUserData(user).then((x) => x[key]);
    }

    async addKey(user: Types.MaybeUser, key: Types.Key, opts: Types.KeyGenOptions): Promise<boolean> {
        const keys = await this.getUserDataValue(user, 'keys');
        return Promise.all([this.setUserDataValue(user, 'keys', {...keys, [key]: opts}), this.client.sadd(KEY_SET_ROOTKEY, key).then(Boolean)]).then(allTrue);
    }

    async removeKey(user: Types.MaybeUser, key: Types.Key): Promise<boolean> {
        const keys = await this.getUserDataValue(user, 'keys');
        delete keys[key];
        return Promise.all([this.setUserDataValue(user, 'keys', keys), this.client.srem(KEY_SET_ROOTKEY, key).then(Boolean)]).then(allTrue);
    }

    async exists(type: 'Username' | 'Key' | 'UUID' | 'Email' | '', key: Types.IdTypes): Promise<boolean> {
        switch (type) {
            case 'Username':
                return this.usernameExists(key as Types.Username);
            case 'Key':
                return this.keyExists(key as Types.Key);
            case 'UUID':
                return this.uuidExists(key as Types.UUID);
            case 'Email':
                return this.emailExists(key as Types.Email);
        }
        return false;
    }
}
