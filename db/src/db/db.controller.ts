import {Controller, Get, Body, UseGuards, Post} from '@nestjs/common';
import {DBService} from 'src/db/db.service';

import {Guards, StatusController, Types} from 'src/shared';

@Controller('db')
@UseGuards(new Guards.InternalCallGuard())
export class DBController extends StatusController {
    constructor(private readonly dbService: DBService) {
        super([]);
    }

    @Post('flush')
    async flush(): Promise<boolean> {
        return this.dbService.flush();
    }

    @Get('exists')
    async exists(@Body('type') type: 'Username' | 'Key' | 'Email' | 'UUID', @Body('key') key: Types.IdTypes): Promise<boolean> {
        return this.dbService.exists(type, key);
    }

    @Post('user_update')
    async updateUserData(@Body('user') user: Types.Username | Types.UUID, @Body('data') data: Partial<Types.UserData>): Promise<boolean> {
        return this.dbService.updateUserData(user, data);
    }

    @Post('user_setValue')
    async setUserDataValue<T extends Types.UserDataKey>(
        @Body('user') user: Types.MaybeUser,
        @Body('key') key: T,
        @Body('data') data: Types.UserData[T],
    ): Promise<boolean> {
        return this.dbService.setUserDataValue(user, key, data);
    }

    @Get('user_getValue')
    async getUserDataValue<T extends Types.UserDataKey>(@Body('user') user: Types.MaybeUser, @Body('key') key: T): Promise<Types.UserData[T]> {
        return this.dbService.getUserDataValue(user, key);
    }

    @Post('key_add')
    async addKey(@Body('user') user: Types.MaybeUser, @Body('key') key: Types.Key, @Body('opts') opts: Types.KeyGenOptions): Promise<boolean> {
        return this.dbService.addKey(user, key, opts);
    }

    @Post('key_remove')
    async removeKey(@Body('user') user: Types.MaybeUser, @Body('key') key: Types.Key): Promise<boolean> {
        return this.dbService.removeKey(user, key);
    }
}
