import {Module} from '@nestjs/common';
import {DBService} from 'src/db/db.service';
import {DBController} from 'src/db/db.controller';
import {RedisModule} from 'nestjs-redis';

import {config} from 'src/shared';

const {host, port, db} = config.redis;

@Module({
    imports: [RedisModule.register({host, port, db})],
    controllers: [DBController],
    providers: [DBService, DBService],
})
export class DBModule {}
