import {DBModule} from './db/db.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(DBModule, 'db');
})();
