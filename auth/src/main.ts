import {AuthModule} from './auth/auth.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(AuthModule, 'auth');
})();
