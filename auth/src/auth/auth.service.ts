import {Injectable} from '@nestjs/common';
import {Hashing, Utils, Types} from 'src/shared';

@Injectable()
export class AuthService {
    async validateUser(username: Types.MaybeUser, password: Types.Password): Promise<boolean> {
        const hash = (await Utils.callInternalGET('db', 'user_getValue', {user: username, key: 'password'})) as Types.Hashed<Types.Password>;
        const valid = await Hashing.compareToHash(hash, password);
        return valid;
    }
}
