import {Controller, Get, Body, UseGuards} from '@nestjs/common';
import {AuthService} from 'src/auth/auth.service';

import {User, Pipes, Guards, Types, StatusController} from 'src/shared';

@Controller('auth')
export class AuthController extends StatusController {
    constructor(private readonly authService: AuthService) {
        super(['db']);
    }

    @UseGuards(new Guards.InternalCallGuard())
    @Get('verify')
    async verifyPassword(
        @Body('password', Pipes.ParsePasswordPipe) password: Types.Password,
        @Body('username', Pipes.UserExistsPipe) user: User,
    ): Promise<boolean> {
        return this.authService.validateUser(user, password);
    }
}
