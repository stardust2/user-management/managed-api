import {Module} from '@nestjs/common';
import {AuthService} from 'src/auth/auth.service';
import {AuthController} from 'src/auth/auth.controller';
import {LocalStrategy} from 'src/shared';
import {PassportModule} from '@nestjs/passport';

@Module({
    imports: [PassportModule],
    controllers: [AuthController],
    providers: [AuthService, LocalStrategy],
})
export class AuthModule {}
