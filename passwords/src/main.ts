import {PasswordsModule} from './passwords/passwords.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(PasswordsModule, 'passwords');
})();
