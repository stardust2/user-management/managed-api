import {Controller, Post, Body, UseGuards} from '@nestjs/common';
import {PasswordsService} from 'src/passwords/passwords.service';

import {User, Types, Guards, StatusController, Pipes} from 'src/shared';
import {AuthGuard} from '@nestjs/passport';

@Controller('passwords')
export class PasswordsController extends StatusController {
    constructor(protected readonly passwordsService: PasswordsService) {
        super(['db']);
    }

    @UseGuards(AuthGuard('local'))
    @Post('change')
    async changePassword(
        @Body('newPassword', Pipes.ParsePasswordPipe) newPassword: Types.Password,
        @Body('username', Pipes.UserExistsPipe) user: User,
    ): Promise<{ success: boolean }> {
        const success = await this.passwordsService.storePassword(user, newPassword);
        return {success};
    }

    @UseGuards(new Guards.InternalCallGuard())
    @Post('store')
    async storePassword(
        @Body('password', Pipes.ParsePasswordPipe) password: Types.Password,
        @Body('username', Pipes.UserExistsPipe) user: User,
    ): Promise<{ success: boolean }> {
        const success = await this.passwordsService.storePassword(user, password);
        return {success};
    }
}
