import {Module} from '@nestjs/common';
import {PasswordsController} from 'src/passwords/passwords.controller';
import {PasswordsService} from 'src/passwords/passwords.service';

@Module({
    controllers: [PasswordsController],
    providers: [PasswordsService],
})
export class PasswordsModule {}
