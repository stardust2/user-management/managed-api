import {Injectable} from '@nestjs/common';

import {Utils, Types, Hashing, User} from 'src/shared';

@Injectable()
export class PasswordsService {
    async storePassword(user: User, password: Types.Password): Promise<boolean> {
        const passwordHash = await Hashing.hashString(password);
        return Utils.callInternalPOST('db', 'user_setValue', {user, key: 'password', data: passwordHash});
    }
}
