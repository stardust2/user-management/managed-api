export interface EmailMsgDto {
    to: string;
    from: string;
    subject: string;
    text: string;
    html: string;
}
