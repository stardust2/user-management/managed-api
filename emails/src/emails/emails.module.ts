import {Module} from '@nestjs/common';
import {EmailController} from 'src/emails/emails.controller';
import {EmailService} from 'src/emails/emails.service';
import {LocalStrategy} from 'src/shared';
import {PassportModule} from '@nestjs/passport';

@Module({
    imports: [PassportModule],
    controllers: [EmailController],
    providers: [EmailService, LocalStrategy],
})
export class EmailModule {}
