import {Injectable} from '@nestjs/common';
import sendgridMail from '@sendgrid/mail';

import {Utils, User, Types, config} from 'src/shared';
import {EmailMsgDto} from 'src/dto/email-msg.dto';

const {defaultFrom, apiKey} = config.email;

@Injectable()
export class EmailService {
    /**
     *
     * @param msg The email data to send
     *
     * Use this as the base method to send an email.
     * TODO add the correct api key
     */
    async handleSendEmail(msg: EmailMsgDto): Promise<void> {
        sendgridMail.setApiKey(apiKey); // TODO: Move to constructor
        try {
            await sendgridMail.send({from: defaultFrom, ...msg});
        } catch (error) {
            console.error(error);
            if (error.response) {
                console.error(error.response.body);
            }
        }
    }

    async storeEmail(user: User, email: Types.Email): Promise<boolean> {
        // const emailHash = await Hashing.hashString(email);
        await Utils.callInternalPOST('db', 'user_setValue', {user: email, key: 'uuid', data: user.uuid});
        return Utils.callInternalPOST('db', 'user_setValue', {user, key: 'email', data: email});
    }

    async changeEmail(user: User, newEmail: Types.Email): Promise<boolean> {
        await Utils.callInternalPOST('db', 'user_setValue', {user: user.email, key: 'uuid', data: ''});
        return this.storeEmail(user, newEmail);
    }

    async resendVerification(_user: User): Promise<boolean> {
        return true; // TODO
    }
}
