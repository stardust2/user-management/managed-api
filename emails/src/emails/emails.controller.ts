import {Controller, Post, Body, UseGuards} from '@nestjs/common';
import {EmailService} from 'src/emails/emails.service';

import {User, Pipes, Types, Guards, StatusController} from 'src/shared';
import {AuthGuard} from '@nestjs/passport';

@Controller('emails')
export class EmailController extends StatusController {
    constructor(private readonly emailsService: EmailService) {
        super(['db']);
    }

    @UseGuards(AuthGuard('local'))
    @Post('change')
    async changeEmail(
        @Body('email', Pipes.ParseEmailPipe) email: Types.Email,
        @Body('username', Pipes.UserExistsPipe) user: User,
    ): Promise<{ success: boolean }> {
        const success = await this.emailsService.changeEmail(user, email);
        return {success};
    }

    @Post('resend_verification') async resendVerification(@Body('username', Pipes.UserExistsPipe) user: User): Promise<boolean> {
        return this.emailsService.resendVerification(user);
    }

    @UseGuards(new Guards.InternalCallGuard())
    @Post('store')
    async storeEmail(
        @Body('email', Pipes.ParseEmailPipe) email: Types.Email,
        @Body('username', Pipes.UserExistsPipe) user: User,
    ): Promise<{ success: boolean }> {
        const success = await this.emailsService.storeEmail(user, email);
        return {success};
    }
}
