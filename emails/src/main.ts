import {EmailModule} from './emails/emails.module';
import {init} from 'src/shared';

(async () => {
    const app = await init(EmailModule, 'emails');
})();
