import {safeLoad} from 'js-yaml';

import * as fs from 'fs';

type Config = {
    http: {
        proxyEnabled: boolean,
        host: string,
        protocol: string
    },
    misc: {
        ports: {[key: string]: number}
    },
    redis: {
        host: string,
        port: number,
        db: number,
        roots: {[key: string]: string}
    },
    email: {
        apiKey: string,
        defaultFrom: string
    },
    secrets: {
        location: string
    }
};

export const config = safeLoad(fs.readFileSync(process.env.CONFIG_LOCATION || `${__dirname}/config.yaml`, 'utf8')) as Config;