import {Strategy} from 'passport-local';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException, BadRequestException} from '@nestjs/common';
import {Types} from '../types';

import validator from 'validator';

import {Utils} from '../utils';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    async validate(username: Types.MaybeUser, password: Types.Password): Promise<any> {
        if (validator.isEmpty(password)) {
            throw new BadRequestException('Password was missing');
        }
        if (typeof username === 'string' && validator.isEmpty(username)) {
            throw new BadRequestException('Username was missing');
        }
        if (!(await Utils.callInternalGET('db', 'exists', {type: 'Username', key: username}))) {
            throw new BadRequestException('User not found');
        }
        const user = await Utils.callInternalGET('auth', 'verify', {username, password});
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
