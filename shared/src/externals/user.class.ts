import validator from 'validator';

import {UUID} from './uuid.class';
import {Types} from '../types';

const isString = (user: any): user is string => typeof user === 'string';

export interface UserI {
    username: Types.Username;
    uuid: Types.UUID;
    email: Types.Email;
}

export class User implements UserI {
    public username: Types.Username;

    public uuid: Types.UUID;

    public email: Types.Email;

    constructor({username, email, uuid}: { username?: Types.Username; email?: Types.Email; uuid?: Types.UUID }) {
        this.username = username || ('' as Types.Username);
        this.uuid = uuid || ('' as Types.UUID);
        this.email = email || ('' as Types.Email);
    }

    merge(other: Partial<User>): User {
        return new User({username: this.username || other.username, email: this.email || other.email, uuid: this.uuid || other.uuid});
    }

    public static isEmail(user: Types.MaybeUser): user is Types.Email {
        return isString(user) && validator.isEmail(user);
    }

    public static isUUID(user: Types.MaybeUser): user is Types.UUID {
        return isString(user) && UUID.isUUID(user);
    }

    public static isUsername(user: Types.MaybeUser): user is Types.Username {
        return isString(user) && !this.isUUID(user) && !this.isEmail(user);
    }
}
