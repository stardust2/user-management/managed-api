import {randomBytes} from 'crypto';
import base64url from 'base64url';
import {User} from './user.class';
import {Types} from '../types';

export class Keys {
    static generateKeyString(_user: User): Types.Key {
        const baseKey = randomBytes(64);
        const newKey = base64url.fromBase64(baseKey.toString('base64')) as Types.Key;
        return newKey;
    }
}
