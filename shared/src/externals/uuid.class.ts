import validator from 'validator';
import {Types} from '../types';

import {v4} from 'uuid';

export class UUID {
    static generate(): Types.UUID {
        return v4() as Types.UUID;
    }

    static isUUID(uuid: string): boolean {
        return validator.isUUID(uuid as Types.UUID, 4);
    }
}
