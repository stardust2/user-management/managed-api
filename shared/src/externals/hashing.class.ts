import * as bcrypt from 'bcrypt';

import {Types} from '../types';

export class Hashing {
    private static saltRounds = 10;

    static async hashString<T extends Types.Hashable>(data: T): Promise<Types.Hashed<T>> {
        return (bcrypt.hash(data, this.saltRounds) as unknown) as Types.Hashed<T>;
    }

    static async compareToHash<T extends Types.Hashable>(hash: Types.Hashed<T>, data: T): Promise<boolean> {
        return bcrypt.compare(data, hash);
    }
}
