import {Get} from '@nestjs/common';

import {Utils, ServiceName} from '../utils';

type StatusData = number;

const validStatus = (x) => !!x;
const mergeAll = <T>(items: T[]): T => items.reduce((prev, curr) => ({...prev, ...curr}), {} as T);

export class StatusController {
    private startDate: number;

    constructor(private readonly deps: ServiceName[]) {
        this.startDate = Date.now();
        setInterval(() => this.assertDepsOnline(), 5000);
    }

    @Get('status')
    async status(): Promise<StatusData> {
        return Date.now() - this.startDate;
    }

    async assertDepsOnline(): Promise<void> {
        const statuses = await this.depsOnline();
        const offlineServices = Object.keys(statuses).filter((key) => !statuses[key]);
        if (offlineServices.length > 0) {
            throw new Error(`The following dependencies were offline: ${offlineServices}!`);
        }
    }

    async depOnline(dep: ServiceName): Promise<{ [dep: string]: boolean }> {
        if (!this.deps.includes(dep)) {
            return {[dep]: false};
        }
        return Utils.callInternalGET(dep, 'status', {})
            .then(validStatus)
            .catch(() => false)
            .then((status) => ({[dep]: status}));
    }

    async depsOnline(): Promise<{ [dep: string]: boolean }> {
        const deps = Promise.all(this.deps.map((dep) => this.depOnline(dep))).then(mergeAll);
        return deps;
    }
}
