import {PipeTransform, Injectable, BadRequestException} from '@nestjs/common';
import validator from 'validator';
import {Types} from '../types';

@Injectable()
export class ParseEmailPipe implements PipeTransform {
    async transform(email: Types.Email): Promise<string> {
        if (typeof email !== 'string' || !validator.isEmail(email)) {
            throw new BadRequestException('Email was not valid');
        }
        return email;
    }
}
