import {PipeTransform, Injectable, BadRequestException} from '@nestjs/common';
import {Types} from '../types';
import {Utils} from '../utils';

@Injectable()
export class KeyExistsPipe implements PipeTransform {
    async transform(value: Types.Key): Promise<string> {
        const exists = await Utils.callInternalGET('db', 'exists', {type: 'Key', key: value});
        if (!exists) {
            throw new BadRequestException('Key was not in the db');
        }
        return value;
    }
}
