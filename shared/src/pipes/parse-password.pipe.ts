import {PipeTransform, Injectable, BadRequestException} from '@nestjs/common';
import {Types} from '../types';

import pwdValidator = require('password-validator');

const schema = new pwdValidator().is().min(8).is().max(128).has().uppercase().has().lowercase().has().digits().has().symbols();

@Injectable()
export class ParsePasswordPipe implements PipeTransform {
    async transform(password: Types.Password): Promise<Types.Password> {
        if (!schema.validate(password)) {
            throw new BadRequestException('Password was not valid, please use at least 1 number, uppercase letter and 1 symbol. Between 8 and 128');
        }
        return password;
    }
}
