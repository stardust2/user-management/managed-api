import {PipeTransform, Injectable, BadRequestException} from '@nestjs/common';
import {User} from '../externals/user.class';
import {Types} from '../types';

import {Utils} from '../utils';

@Injectable()
export class EmailNotExistsPipe implements PipeTransform {
    async transform(email: Types.Email): Promise<User> {
        const exists = await Utils.callInternalGET('db', 'exists', {type: 'Email', key: email});
        if (exists) {
            throw new BadRequestException('Email was already taken');
        }
        const user = new User(await Utils.callInternalGET('db', 'user_getValue', {user: email, key: '*'}));
        return user.merge({email});
    }
}
