import {EmailNotExistsPipe} from './email-not-exists.pipe';
import {UserNotExistsPipe} from './user-not-exists.pipe';
import {UserExistsPipe} from './user-exists.pipe';
import {KeyExistsPipe} from './key-exists.pipe';
import {ParseEmailPipe} from './parse-email.pipe';
import {ParsePasswordPipe} from './parse-password.pipe';

export const Pipes = {EmailNotExistsPipe, UserNotExistsPipe, UserExistsPipe, KeyExistsPipe, ParseEmailPipe, ParsePasswordPipe};
