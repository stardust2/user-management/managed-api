import {PipeTransform, Injectable, BadRequestException} from '@nestjs/common';
import {Types} from '../types';
import {User} from '../externals/user.class';
import {Utils} from '../utils';

@Injectable()
export class UserExistsPipe implements PipeTransform {
    async transform(username: Types.Username): Promise<User> {
        const exists = await Utils.callInternalGET('db', 'exists', {type: 'Username', key: username});
        if (!exists) {
            throw new BadRequestException('User was not signed up');
        }
        const user = new User(await Utils.callInternalGET('db', 'user_getValue', {user: username, key: '*'}));
        return user.merge({username});
    }
}
