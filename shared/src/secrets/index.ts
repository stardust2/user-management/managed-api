import * as fs from 'fs';

import {config} from 'src/shared/config';

const secretsFolder = config.secrets.location;

export const httpsOptions = {
    key: fs.readFileSync(`${secretsFolder}/localhost.key`),
    cert: fs.readFileSync(`${secretsFolder}/localhost.crt`),
};
