import Axios from 'axios';
import * as https from 'https';
import {config} from './config';

const httpsAgent = new https.Agent({rejectUnauthorized: false});

const {proxyEnabled, host, protocol} = config.http;

const port = (key: ServiceName) => (proxyEnabled ? '' : `:${config.misc.ports[key]}`);

const url = (key: ServiceName, route: string) => `${host}${port(key)}/${key}/${route}`;

export type ServiceName = keyof typeof config.misc.ports;
const callInternalPOST = async <T, U>(key: ServiceName, route: string, data: T): Promise<U> => {
    const res = await Axios.post(`${protocol}://${url(key, route)}`, data, {httpsAgent});
    return res.data;
};

const callInternalGET = async <T, U>(key: ServiceName, route: string, data: T = {} as T): Promise<U> => {
    const res = await Axios.get(`${protocol}://${url(key, route)}`, {httpsAgent, data});
    return res.data;
};

export const Utils = {callInternalGET, callInternalPOST};
