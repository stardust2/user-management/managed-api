import {Injectable, CanActivate, ExecutionContext, applyDecorators, UseGuards, Get} from '@nestjs/common';

@Injectable()
export class InternalCallGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> {
        const {
            connection: {localAddress, remoteAddress},
        } = context.switchToHttp().getRequest();
        return localAddress === remoteAddress;
    }
}

export function internalGet(route: string) {
    return applyDecorators(UseGuards(new InternalCallGuard()), Get(route));
}
