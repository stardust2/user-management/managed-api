import {NestFactory} from '@nestjs/core';
import * as helmet from 'helmet';

import {httpsOptions} from './secrets';
import {portMap} from './config';

import './failOnPromiseRejection';
import {INestApplication} from '@nestjs/common';

export const init = async (module, key): Promise<INestApplication> => {
    const app = await NestFactory.create(module, {httpsOptions});
    app.use(helmet());
    app.enableCors();
    return app.listen(process.env.PORT || portMap[key]);
};
