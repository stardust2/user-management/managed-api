import {User, UserI} from './externals/user.class';

type Brand<T, BrandType, BrandName extends string = '__type__'> = T & { [K in BrandName]: BrandType };

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace Types {
    export type Hashable<T = string> = Brand<T, true, '__hashable__'>;
    export type Hashed<T extends Hashable> = Brand<T, true, '__hashed__'>;

    export type Key = Brand<string, 'apikey'>;
    export type UUID = Brand<string, 'UUID'>;
    export type Username = Brand<string, 'username'>;
    export type Password = Brand<Hashable, 'password'>;
    export type Email = Brand<Hashable, 'email'>;

    export type KeysObj = { [key: string /*Key*/]: KeyGenOptions };

    export interface KeyData extends KeyGenOptions {
        key: Key;
    }

    export interface UserData {
        password: Hashed<Password>;
        uuid: UUID;
        email: Email;
        username: Username;
        keys: KeysObj;
        misc: any;
        ['*']: UserData;
    }

    export enum Scope {
        FULL,
    }

    export interface KeyGenOptions {
        ttl: number;
        uses: number;
        end: number;
        scope: Scope;
    }

    export type MaybeUser = User | UUID | Username | Email | UserI;

    export type IdTypes = Username | Email | UUID | Key;

    export type UserDataKey = keyof UserData;
}
